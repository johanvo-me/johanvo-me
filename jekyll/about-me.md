---
layout: default
title: About me
---

I'm Johan van Overbeeke, a computer programmer from [Utrecht](https://en.wikipedia.org/wiki/Utrecht). I live there with 
Nicole Wezelman, and the amount of happiness we bring each other is something I wish upon everyone :-).

I am generally a private person and like learning and building things a lot more than writing about myself, so the rest
of this website will be about (software) stuff I've made.


### About building software

From a young age I have enjoyed building things and finding out how stuff works. This started with my toys, but the 
number of new and interesting ways to play with them always quickly diminished. A second hand Commodore 64 was the 
first toy that never ran out of new things to try. I have been hooked ever since!

One of the things I love most about software design is the endless amount there is to learn and improve myself with. 
Therefore it's no surprise that I spend a lot of my free time fiddling with new tools and techniques. I appreciate 
all parts of the process, from the first glimpses into the problem domain to the running of a stable 
production environment.


I enjoy working in an environment that values and invests in continued improvement, both of the product and their 
employees. As fun as studying in my own time is, I've found that it is no replacement for having study and 
improvement as an integrated part of the business culture. Learning on your own covers different ground than 
learning as a group for actual business goals. Besides that, studying as a team helps prevent knowledge silos and 
keeps everyone speaking the same language.


That last part is especially important because creating really great software is a team effort. You need differing 
perspectives and the ability to voice/hear them to create a product that gets the most out of the available 
technology and meets the customer's needs. I think technical quality is part of the business value of a system; 
a high quality system gives more value for less maintenance costs and is quicker to adapt to changing 
business requirements.
