---
layout: default
title: Projects
permalink: /projects/
---
# Projects

This are some projects I've done. Ideally I'd give each project a full writeup with goals/description/conclusion/etc.,
but as I prefer writing code over writing project pages we'll see how that goes :-P.

The projects:

{% for project in site.projects %}
  <h2>
    <a href="{{ project.url }}"> {{ project.name }} </a>
  </h2>
  <p>
    {{ project.summary }}
    <small><small>
      ( {{ project.time-period }} )
    </small></small>
  </p>
{% endfor %}
