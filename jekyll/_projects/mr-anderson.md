---
name: mr-anderson
title: mr-anderson
summary: Server load testing experiments.
time-period: december 2018 to now
layout: page
---
"Mr. Anderson" is a personal experiment with different approaches to handling high amounts of traffic with a slow 
persistence mechanism that cannot itself be changed.

For more details see the [mr-anderson project page](http://mr-anderson.johanvo.me/) .

## Links
* [project page](http://mr-anderson.johanvo.me/)
* [Gitlab repository](https://gitlab.com/mr-anderson/mr-anderson)