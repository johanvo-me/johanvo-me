---
name: gogodot
title: gogodot
summary: Easy publication of Godot game
time-period: october 2018
layout: page
---
[Godot](https://godotengine.org/) is a high quality, multi-platform, open source game engine. 
The goal of gogodot is to make your Godot game available to the world for most of its supported platforms (along with
a simple website), while taking as little of your time possible.

The name is meant to resemble the "Go! Go! Go!" call for action, but with "Godot" mixed in. Because it is about 
speedy/hurried Godot development.

For more details see the [project page](https://gitlab.com/Anarril/gogodot).

## Links
* [Gitlab repository](https://gitlab.com/Anarril/gogodot)
* [Example output](https://anarril.gitlab.io/gogodot/)
