---
name: boxx-localhost
title: boxx-localhost
summary: Development environment for Omniboxx.
time-period: first half of 2017
layout: page
---

Vagrant and Puppet based development environment for use at my job developing the [Omniboxx real-estate management software](https://www.omniboxx.nl/).
The goal was to have easy access to a good dev environment that was quick to set up and was as similar as possible to the 
production one (to limit environment discrepancy bugs).

Initially the output of [PuPHPet](https://puphpet.com/), this project was heavily customized to enable installation with
a few simple instructions, improve Omniboxx development support and match the Omniboxx production environment as close 
as possible. This included things like:

* scripts to install requirements such as vagrant, virtual box, etc.
* automatically install Omniboxx during installation, including config files etc.
* static code analysis tools
* enable git-flow by default and include auto-complete for it on the CLI
* performance monitoring dashboard

Some more details can be seen in the [project repository](https://gitlab.com/Anarril/boxx_localhost), however you can only create
a running Omniboxx installation if you have access to the Omniboxx Github repository :-P.

## Links
* [Gitlab repository](https://gitlab.com/Anarril/boxx_localhost)
* Only if you have the vagrant machine running:
    * Overview page: [http://boxx.localhost](http://boxx.localhost)
    * Omniboxx installation: [http://omni.boxx.localhost](http://omni.boxx.localhost)
    * Static analysis metrics: [http://metrics.boxx.localhost](http://metrics.boxx.localhost)
    * Freely usable sandbox web root: [http://sand.boxx.localhost](http://sand.boxx.localhost)
