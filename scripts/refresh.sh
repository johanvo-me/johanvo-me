#!/bin/sh

cd ../jekyll && \
docker run --rm \
        --volume="$PWD:/srv/jekyll" \
        --volume="$PWD/vendor/bundle:/usr/local/bundle" \
        -t jekyll/jekyll:3.8 \
        jekyll build . && \
cd .. && \
docker build -t johanvo-me . && \
docker stop johanvo-me-container;
docker rm johanvo-me-container;
docker run --name johanvo-me-container -d -p 80:80 johanvo-me
