#!/bin/sh

cd ../jekyll && \
docker run --rm \
        --volume="$PWD:/srv/jekyll" \
        --volume="$PWD/vendor/bundle:/usr/local/bundle" \
        -p 80:8080 \
        -p 35729:35729 \
        -t jekyll/jekyll:3.8 \
        jekyll serve --watch --livereload --host 0.0.0.0 --port 8080
