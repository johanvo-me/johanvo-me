FROM nginx:1.15

COPY conf/nginx/x-clacks-overhead.conf /etc/nginx/conf.d/x-clacks-overhead.conf

COPY jekyll/_site /usr/share/nginx/html
